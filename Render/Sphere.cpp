//
//  Sphere.cpp
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#include <iostream>

#include "Sphere.h"

bool Sphere::Intersect( Ray &ray) {
    
    Vector dist = pos - ray.origin;
    float B = Dot( dist, ray.dir);
    float D = B*B - Dot(dist, dist) + radius * radius;
    if( D < 0.0f )
        return false;
    D = sqrtf( D );
    float t0 = B - D;
    float t1 = B + D;
    
    if( ( t0 > 0.1f ) && (t0 < ray.t ) ){
        ray.t = t0;
        return true;
    }
    if( ( t1 > 0.1f) && (t1 < ray.t ) ) {
        ray.t = t1;
        return true;
    }
    return false;
}

Vector Sphere::getNormal( const Point point ) const {
    return point - pos;
}