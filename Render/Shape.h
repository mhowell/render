//
//  Shape.h
//  Render
//
//  Created by Matthew Howell on 20/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_Shape_h
#define Render_Shape_h

#include "geometry.h"
#include "Material.h"

class Shape {

public:
    Material mat;
    
    Material getMaterial() { return mat; }
    
    void setMaterial(Material mat ) { mat = mat; }
};


#endif
