//
//  main.cpp
//  Render
//
//  Created by Matthew Howell on 20/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#include <iostream>

#include "Raytrace.h"
#include "Sphere.h"

int main (int argc, const char * argv[])
{

    // insert code here...
    std::cout << "Hello, World!\n";
    
    
    Scene scene;
    
    Material mat1;
    mat1.setRelfection(0.6f);
    mat1.setDiffues( 1.0f );
    mat1.setColour( Colour( 0.4f, 0.3f, 0.3f) );
    
    Material mat2;
    mat2.setRelfection(1.0f);
    mat2.setDiffues(0.1f);
    mat2.setColour( Colour( 0.7, 0.7, 1.0f) );
    
    Point p1( 1.0, -0.8, 3 );
    Sphere sphere1(p1, 2.5, mat1 );
    
    scene.geometryList.push_back(sphere1);

    Point p2(-5.5, -0.5, 7);
    Sphere sphere2( p2, 2, mat2 );
    
    scene.geometryList.push_back(sphere2);
    
    Point pl1( 0, 5, 5 );
    Colour cpl1( 0.4f, 0.4f, 0.4f );
    Light light1( cpl1, pl1 );
    
    scene.lightList.push_back( light1 );
    
    Point pl2( 2, 5, 1 );
    Colour cpl2( 0.6f, 0.6f, 0.8f );
    Light light2( cpl2, pl2 );
    
    scene.lightList.push_back(light2);
    
    Raytracer raytracer;
    
    raytracer.setScene( scene );
    
    raytracer.setSize( 640, 480 );
    
    raytracer.RenderFrame();
    
    return 0;
}

