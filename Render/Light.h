//
//  Light.h
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_Light_h
#define Render_Light_h

#include "geometry.h"

class Light {
public:
    Colour colour;
    Point pos;
    
    Light( Colour colour, Point pos )
    : colour( colour), pos(pos) {}
    
    Colour getColour() { return colour; }
    Point getPos() { return pos; }
};


#endif
