//
//  Sphere.h
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_Sphere_h
#define Render_Sphere_h

#include "geometry.h"

class Sphere
public:
    
    Point pos;
    float radius;

    Sphere( Point pos, float radius, Material mat )
    : pos(pos), radius(radius), mat(mat) {
    }
    
    bool Intersect( Ray &ray );
    Vector getNormal( const Point point ) const;
    
};

s
#endif
