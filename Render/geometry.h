//
//  geometry.h
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_geometry_h
#define Render_geometry_h

#include <cmath>

#define EPSILON			0.0001f
#define MAX_TRACEDEPTH  10

class Vector {
public:
    float x, y, z;
    
    Vector() { x = y = z = 0.f; }
    Vector( float x, float y, float z )
    : x(x), y(y), z(z) {}
    
    //Overrides
    
    Vector operator+(const Vector &v) const {
        return Vector(x + v.x, y + v.y, z + v.z );
    }
    
    
    Vector& operator+=(const Vector &v) {
        x += v.x; y += v.y; z += v.z;
        return *this;
    }
    
    Vector operator-(const Vector &v) const {
        return Vector(x-v.x, y-v.y, z-v.z);
    }
    
    Vector& operator-=(const Vector &v) {
        x -= v.x; y -= v.y; z -= v.z;
        return *this;
    }
    
    Vector operator*(float f) const {
        return Vector(f*x, f*y, f*z); 
    }
    
    Vector& operator*=(float f) {
        x *= f; y *= f; z *= f;
        return *this;
    }
    
    Vector operator/(float f) const {
        float inv = 1.f / f;
        return Vector( x * inv, y * inv, z * inv );
    }
    
    Vector &operator/=(float f) {
        float inv = 1.f / f;
        x *= inv; y *= inv; z *= inv;
        return *this;
    }
    
    float LengthSquared() const { return x*x + y+y + z*z; }
    float Length() const { return sqrtf(LengthSquared()); }
    
    Vector Normalize() {
        return *this / this->Length();
    }
    

};

inline Vector operator*(float f, const Vector &v) 
{ 
    return v*f;
}

inline float Dot( const Vector &v1, const Vector &v2 ) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

class Point {
public:
    float x, y, z;
    
    Point() { x = y = z = 0.f; }
    Point(float x, float y, float z)
    : x(x), y(y), z(z) {}
    
    Point operator+(const Vector &v)const {
        return Point( x + v.x, y + v.y, z + v.z );
    }
    
    Vector operator-(const Point &p) const {
        return Vector( x - p.x, y - p.x, z - p.z);
    }

};

class Ray {
public:
    Point origin;
    Vector dir;
    float t;
    int depth;
    
    Ray() : t(0) {}
    Ray( const Point &origin, const Vector &dir, float t, float depth = 0 )
    : origin(origin), dir(dir), t(t), depth(depth) {}
};

class Colour {
public:
    float r, g, b, a;
    
    Colour() { r = g = b = 0.f; a = 1; }
    Colour( float r, float g, float b, float a = 1 )
    : r(r), g(g), b(b), a(a) {}
    
    
    Colour operator*(float f) const {
        return Colour(f*r, f*g, f*b); 
    }
    
    Colour& operator*=(float f) {
        r *= f; g *= f; b *= f;
        return *this;
    }
    
    Colour operator+(const Colour &c) const {
        return Colour(r + c.r, g + c.g, b + c.b );
    }
    
    
    Colour& operator+=(const Colour &c) {
        r += c.r; g += c.g; b += c.b;
        return *this;
    }
    
    Colour operator*(const Colour &c ) {
        return Colour( r * c.r, g * c.g, b * c.b );
    }
    
    friend Colour operator*(const Colour& c1, Colour& c2 )
    {
        return Colour( c1.r * c2.r, c1.g * c2.g, c1.b * c2.b );
    }
    
};

inline Colour operator*(float f, const Colour &c) 
{ 
    return c*f;
}


#endif
