//
//  Material.h
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_Material_h
#define Render_Material_h

#include "geometry.h"

class Material {
public:
    Colour colour;
    float reflection;
    float diffuse;
    
    Material() { reflection = diffuse = 0.f; }
    Material( Colour colour, float reflection, float diffues )
    : colour(colour), reflection(reflection), diffuse(diffuse) {}
    
    void setRelfection( float reflection ) { reflection = reflection; }
    float getReflection() { return reflection; }
    void setDiffues( float diffuse ) { diffuse = diffuse; }
    float getDiffuse() { return diffuse; }
    float getSpecular() { return 1.0f - diffuse; }
    void setSpecular( float specular ) { specular = specular; }
    Colour getColour() { return colour; }
    void setColour( Colour colour ) { colour = colour; }
    
};


#endif
