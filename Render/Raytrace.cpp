//
//  Raytrace.cpp
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#include <iostream>
#include <FreeImagePlus.h>

#include "Raytrace.h"


void Raytracer::setScene( Scene scene ) {
    scene = scene;
}

void Raytracer::setSize( int width, int height ) {
    width = width;
    height = height;
}

Colour Raytracer::Raytrace( Ray &ray ) {
    Sphere *hitSphere;
    Colour colour;
    
    if( ray.depth > MAX_TRACEDEPTH ) return Colour( 1, 0.0, 1 );
    
    for( int i = 0; i < scene.geometryList.size(); ++i)
    {
        Sphere shape = scene.geometryList[i];
        if( shape.Intersect( ray ) ) {
            hitSphere = &shape;
        }
    }
    
    if( !hitSphere ) return Colour();
    
    Material mat = ((Shape*)hitSphere)->getMaterial();
    
    Point newStart = ray.origin + ray.dir * ray.t;
    
    for( int j = 0; j < scene.lightList.size(); ++j )
    {
        Light light = scene.lightList[j];
        
        Vector L = light.pos - newStart;
        float tdist = L.Length();
        L *= (1.0f / tdist);
        Ray r = Ray(  newStart + L * EPSILON, L, tdist );
        
        bool inShadow = false;
        for( int i = 0; i < scene.geometryList.size(); ++i ) 
        {
            if( scene.geometryList[i].Intersect(r) ) {
                inShadow = true;
                break;
            }
        }
        
        L = light.pos - newStart;
        L.Normalize();
        Vector N = hitSphere->getNormal(newStart);
        

        // Calculate diffuse shading
        if( mat.getDiffuse() > 0 )
        {
            float dot = Dot( L, N );
            if( dot > 0 ) 
            {
                float diff = dot * mat.getDiffuse() * inShadow;
                colour += diff * (light.getColour() * mat.getColour());
            }
        }
        
        if( mat.getSpecular() > 0 )
        {
            Vector V = ray.dir;
            Vector R = L - 2.0f * Dot( L, N ) * N;
            float dot = Dot( V, R );
            if( dot > 0 )
            {
                float spec = powf( dot, 20 ) * mat.getSpecular() * inShadow;
                colour += spec * light.getColour();
            }
        }
    }
    
    float ref = mat.getReflection();
    if( ref > 0.0f ) {
        Vector N = hitSphere->getNormal( newStart );
        Vector R = ray.dir - 2.0f * Dot( ray.dir, N ) * N;
        if( ray.depth < MAX_TRACEDEPTH )
        {
            Point pos = newStart + R * EPSILON;
            Ray refray( pos, R, 2000.f, ray.depth + 1 );
            Colour rcol = Raytrace( refray );
            colour += ref * rcol * mat.getColour();
        }
    }
    
    return colour;
}

void Raytracer::RenderFrame() {
    
    fipImage image(FIT_BITMAP, width, height, 24);
    
    for( int y = 0; y < height; y++ ) {
        for( int x = 0; x < width; x++ ) {
             
            Point p( x, y, -1000.0f );
            Vector v( 0.0f, 0.0f, 1.0f );
            Ray viewRay( p, v, 2000.0f );
            
            Colour col = Raytrace( viewRay );
                      
            RGBQUAD *pixel = new RGBQUAD();
            pixel->rgbRed = col.r;
            pixel->rgbGreen = col.g;
            pixel->rgbBlue = col.b;
            image.setPixelColor(x, y, pixel);
            
        }
    }
    
    image.save("image.bmp");
}