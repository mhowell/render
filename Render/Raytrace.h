//
//  Raytrace.h
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_Raytrace_h
#define Render_Raytrace_h

#include "Scene.h"
#include "Shape.h"
#include "geometry.h"
#include "Material.h"
#include "Light.h"

class Raytracer {
    int width, height;
    Scene scene;
    
public:
    void RenderFrame();
    void setScene(Scene scene);
    void setSize( int width, int height );
    
    Colour Raytrace(Ray &ray);
    
};

#endif
