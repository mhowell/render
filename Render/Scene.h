//
//  Scene.h
//  Render
//
//  Created by Matthew Howell on 21/01/12.
//  Copyright (c) 2012 None. All rights reserved.
//

#ifndef Render_Scene_h
#define Render_Scene_h

#include <vector>

#include "Light.h"
#include "Sphere.h"

class Scene {
public:
    std::vector<Sphere> geometryList;
    std::vector<Light> lightList;
    
};

#endif
